from django.shortcuts import render
from django.http.response import HttpResponse
from django.contrib.auth.hashers import make_password

# Create your views here.
def index(req):
    if req.method == "POST":
        pw = req.POST.get('password','') 
        print(pw)
        res = render(req,"signup.html")  #res라는 변수에    
        res.set_cookie('pw_test',make_password(pw)) # pw_test 라는 변수에 pw의cookie 설정해줌 근데 make_password lib 추가하면 쿠키가 암호화 됨.
        return res


    return render(req, "signup.html")
